const fs = require("fs")

function Callback2(boardId, callback) {
    try {
        if (boardId === undefined || typeof boardId !== "string") {
            throw new Error("Invalid input type for problem 2")
        } else {
            const seconds = Math.floor(Math.random() * 10)

            setTimeout(() => {
                fs.readFile("./lists.json", (err, data) => {
                    if (err) {
                        callback(err.message)
                    } else {
                        const JSONdata = JSON.parse(data)
                        const list = Object.entries(JSONdata)

                        // filtering particular Id from List data
                        const boardIdList = list.filter((id) => {
                            return id[0] === boardId
                        })

                        // Passing control to the function callback
                        if (boardIdList.length == 0) {
                            callback("No such BoardId in Lists")
                        } else {
                            const res = boardIdList.reduce(
                                (accum, currElem) => {
                                    accum[currElem[0]] = currElem[1]
                                    return accum
                                },{})

                            callback(null, res)
                        }
                    }
                })
            }, seconds * 1000)
        }
    } catch (err) {
        console.log(err.message)
    }
}

module.exports = Callback2
