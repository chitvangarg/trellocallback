const fs = require("fs")

function Callback1(boardId, callback) {
    try {
        if (boardId === undefined || typeof boardId !== "string") {
            throw new Error("Invalid input type for problem 2")
        } else {
            const seconds = Math.floor(Math.random() * 10)
            setTimeout(() => {
                fs.readFile("./boards.json", "utf-8", (err, data) => {
                    if (err) {
                        callback(err.message)
                    } else {
                        // Searching for required BoardId
                        let boardsData = JSON.parse(data)
                        const requiredBoard = boardsData.find((board) => {
                            return board.id === boardId
                        })

                        // Pass the data to callback
                        if (requiredBoard === undefined) {
                            callback("No such id Present")
                        } else {
                            callback(null, requiredBoard)
                        }
                    }
                })
            }, seconds * 1000)
        }
    } catch (err) {
        console.log(err.message)
    }
}

module.exports = Callback1
