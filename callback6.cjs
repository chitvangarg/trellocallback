const fs = require("fs")
const Callback1 = require("./callback1.cjs")
const Callback2 = require("./callback2.cjs")
const Callback3 = require("./callback3.cjs")

const Callback6 = function (boardName) {
    try {
        if (boardName === undefined) {
            throw new Error("There must be some argument")
        } else {
            const seconds = Math.floor(Math.random() * 10)

            setTimeout(() => {
                fs.readFile("./boards.json", (err, data) => {
                    if (err) {
                        throw new Error(err.message)
                    } else {
                        // Filtering out required board name
                        const parsedData = JSON.parse(data)

                        const requiredBoard = parsedData.find((board) => {
                            return board.name === boardName
                        })

                        // Using previously made function
                        Callback1(requiredBoard["id"], function (err, data) {
                            if (err) {
                                console.log(err)
                            } else {
                                console.log(data)

                                Callback2(data["id"], function (err, data) {
                                    if (err) {
                                        console.log(err)
                                    } else {
                                        console.log(data)

                                        const lists = data[requiredBoard["id"]]

                                        const listsId = lists.map((elem) => {
                                            return elem["id"]
                                        })

                                        let totalList = 0

                                        while (totalList < listsId.length) {
                                            Callback3(
                                                listsId[totalList],
                                                function (err, data) {
                                                    if (err) {
                                                        console.log(err)
                                                    } else {
                                                        console.log(data)
                                                    }
                                                }
                                            )
                                            totalList++
                                        }
                                    }
                                })
                            }
                        })
                    }
                })
            }, seconds * 1000)
        }
    } catch (err) {
        console.log(err)
    }
}

module.exports = Callback6
