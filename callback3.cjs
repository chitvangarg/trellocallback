const fs = require("fs")

const Callback3 = function (listId, callback) {
    try {
        if (listId === undefined || typeof listId !== "string") {
            throw new Error("Invalid listId")
        } else {
            const seconds = Math.floor(Math.random() * 10)

            setTimeout(() => {
                fs.readFile("./cards.json", (err, data) => {
                    if (err) {
                        callback(err.message)
                    } else {
                        const listData = JSON.parse(data)
                        const cardsId = Object.entries(listData)

                        // Filtering out required ID data
                        const card = cardsId.filter((elem) => {
                            return elem[0] === listId
                        })

                        // Passing control to the function callback
                        if (card.length === 0) {
                            callback("No such ListId in Cards")
                        } else {
                            const result = card.reduce((accum, currElem) => {
                                accum[currElem[0]] = currElem[1]
                                return accum
                            }, {})

                            callback(err, result)
                        }
                    }
                })
            }, seconds * 1000)
        }
    } catch (err) {
        console.log(err.message)
    }
}

module.exports = Callback3
