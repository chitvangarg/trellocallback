const fs = require("fs")
const Callback1 = require("./callback1.cjs")
const Callback2 = require("./callback2.cjs")
const Callback3 = require("./callback3.cjs")

const Callback5 = function (boardName) {
    try {
        if (boardName === undefined) {
            throw new Error("There must be some argument")
        } else {
            const seconds = Math.floor(Math.random() * 10)

            setTimeout(() => {
                fs.readFile("./boards.json", (err, data) => {
                    if (err) {
                        throw new Error(err.message)
                    } else {
                        const parsedData = JSON.parse(data)

                        // Filtering required data
                        const requiredBoard = parsedData.find((board) => {
                            return board.name === boardName
                        })

                        // Using Previously made functions
                        Callback1(requiredBoard["id"], function (err, data) {
                            if (err) {
                                console.log(err)
                            } else {
                                console.log(data)

                                Callback2(data["id"], function (err, data) {
                                    if (err) {
                                        console.log(err)
                                    } else {
                                        console.log(data)

                                        const lists = data[requiredBoard["id"]]
                                        const Mindlist = lists.find((elem) => {
                                            return elem.name === "Mind"
                                        })
                                        const Spacelist = lists.find((elem) => {
                                            return elem.name === "Space"
                                        })

                                        Callback3(
                                            Mindlist["id"],
                                            function (err, data) {
                                                if (err) {
                                                    console.log(err)
                                                } else {
                                                    console.log(data)
                                                }
                                            }
                                        )

                                        Callback3(
                                            Spacelist["id"],
                                            function (err, data) {
                                                if (err) {
                                                    console.log(err)
                                                } else {
                                                    console.log(data)
                                                }
                                            }
                                        )
                                    }
                                })
                            }
                        })
                    }
                })
            }, seconds * 1000)
        }
    } catch (err) {
        console.log(err)
    }
}

module.exports = Callback5
